
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class OrderController : ControllerBase
    {
        private readonly OrderContext _context;

        public OrderController(OrderContext context) {
            _context = context;
        }

        /// <summary>
        /// Busca uma venda por ID
        /// </summary>
        [HttpGet ("GetOrderById{id}")]
        public IActionResult GetById(int id) {

            var order = _context.Orders.Find(id);
            var seller = _context.Sellers.Find(id);
            var item = _context.Products.Find(id);

            if(order == null)
                return NotFound();
                
            return Ok(order);
        }

        /// <summary>
        /// Busca todas as vendas
        /// </summary>
        [HttpGet("GetOrders")]
        public IActionResult GetOrders()
        {
            var orders = _context.Orders;

            if (orders.Count() < 1)
                return NotFound();            

            return Ok(orders.Include( p => p.Seller )
                            .Include( p => p.Items )
                            .ToList());
        }

        /// <summary>
        /// Cria uma nova venda
        /// </summary>
        [HttpPost]
        public IActionResult CreateOrder(Order order)
        {

            if(ModelState.IsValid) {
                Random random = new Random();
                var newON = random.Next(1, 9999);
                var filterON = _context.Orders.Where( x => x.OrderNumber == newON ).Any();

                if(filterON)
                {
                    order.OrderNumber = newON + 1;
                }
                else
                {
                    order.OrderNumber = newON;
                }

                _context.Add(order);
                _context.SaveChanges();

                return Ok("Pedido realizado com sucesso!");
            }
            
            return BadRequest();
        }

        /// <summary>
        /// Atualiza uma venda pelo ID
        /// </summary>
        [HttpPut("{id}")]
        public IActionResult Update(int id, OrderStatus status)
        {
            var order = _context.Orders.Find(id);

            if (order == null)
                return NotFound();

            void save() {
                order.Status = status;
                _context.Orders.Update(order);
                _context.SaveChanges();
            }

            switch(order.Status) {
                case OrderStatus.Aguardando_Pagamento:
                    if( status == OrderStatus.Pagamento_Aprovado) {
                        save();
                        return Ok("Status alterado para Pagamento Aprovado!");
                    } 
                    if( status == OrderStatus.Cancelado) {
                        save();
                        return Ok("Status alterado para Cancelado!");
                    }
                    break;
                    
                case OrderStatus.Pagamento_Aprovado:
                    if( status == OrderStatus.Enviado_Para_Transportadora) {
                        save();
                        return Ok("Status alterado para Enviado para Transportadora");
                    }
                    if( status == OrderStatus.Cancelado) {
                        save();
                        return Ok("Status alterado para Cancelado!");
                    }
                    break;

                case OrderStatus.Entregue:
                    if( status == OrderStatus.Enviado_Para_Transportadora) {
                        save();
                        return Ok("Status alterado para Entregue");
                    }
                    break;
                    
            }

            return  BadRequest($"Impossivel alterar o status de '{order.Status.ToString()}' para '{status.ToString()}'");

        }
    }
}