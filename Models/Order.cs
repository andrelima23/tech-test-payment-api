using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Order
    {
        public Order() {
            this.Status = OrderStatus.Pagamento_Aprovado;
            this.OrderDate = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public Seller Seller { get; set; }

        [Required]
        public int OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        
        [Required]
        public List<Product> Items { get; set; }
        public OrderStatus Status { get; set; }

    }
}
