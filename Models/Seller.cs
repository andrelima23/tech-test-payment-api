using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Seller
    {
        public Seller() {
            
        }

        public Seller(string nome, string cpf, string email, string telefone) {
            this.Nome = nome;
            this.CPF = cpf;
            this.Email = email;
            this.Telefone = telefone;
        }

        public int Id { get; set; }

        /// <example>Andre Lima</example>
        [Required]
        public string Nome { get; set; }

        /// <example>99999999999</example>
        [Required]
        [StringLength(11)]
        public string CPF { get; set; }

        /// <example>vendedor@gitlab.com</example>
        [Required]
        public string Email { get; set; }

        /// <example>91999999999</example>
        [Required]
        [MinLength(10)]
        [MaxLength(11)]
        public string Telefone { get; set; }
    }
}